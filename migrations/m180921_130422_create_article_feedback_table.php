<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article_feedback`.
 */
class m180921_130422_create_article_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article_feedback', [
                'id' => $this->primaryKey(),
                'id_article'=> $this->integer(),
                'author'=> $this->integer(),
                'rating'=> $this->double(),
                'message' => $this->string(),       
                 ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article_feedback');
    }
}
