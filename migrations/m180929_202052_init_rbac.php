<?php

use yii\db\Migration;

/**
 * Class m180929_202052_init_rbac
 */
class m180929_202052_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
    
        $author = $auth->createRole('author');
        $auth->add($author);
  
        $editor = $auth->createRole('editor');
        $auth->add($editor);
  
        $admin = $auth->createRole('admin');
        $auth->add($admin);
                
  
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $editor);
  
  
        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);
  
        $deleteArticle = $auth->createPermission('deleteArticle');
        $auth->add($deleteArticle);

        $deleteFeedback = $auth->createPermission('deleteFeedback');
        $auth->add($deleteFeedback);

        $writeArticle = $auth->createPermission('writeArticle');
        $auth->add($writeArticle);
  
        $updateArticle = $auth->createPermission('updateArticle');
        $auth->add($updateArticle);                    
                
        $updateOwnArticle = $auth->createPermission('updateOwnArticle');
  
        $editArticle = $auth->createPermission('editArticle');
        $auth->add($editArticle);            

        $seeRating = $auth->createPermission('seeRating');
        $auth->add($seeRating);                    
           
        $seeStatus = $auth->createPermission('seeStatus');
        $auth->add($seeStatus);       
      
        $manageTags = $auth->createPermission('manageTags');
        $auth->add($manageTags); 

        $manageCategory = $auth->createPermission('manageCategory');
        $auth->add($manageCategory); 
        
/*
        $updateUsers = $auth->createPermission('updateUsers');
        $auth->add($updateUsers);

        $updateOwnUser = $auth->createPermission('updateOwnUser');
*/
        $rule = new \app\rbac\AuthorRule; 
        $auth->add($rule); 
   
        $updateOwnArticle->ruleName = $rule->name;                
        $auth->add($updateOwnArticle);                 
    /*           
        $rule = new \app\rbac\UserRule;
        $auth->add($rule);
        
        $updateOwnUser->ruleName = $rule->name;                 
        $auth->add($updateOwnUser);
*/
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $deleteFeedback);
        $auth->addChild($editor, $manageCategory);
        $auth->addChild($editor, $deleteArticle);
        $auth->addChild($author, $writeArticle); 
        $auth->addChild($author, $updateOwnArticle); 
        $auth->addChild($updateOwnArticle, $updateArticle);
        $auth->addChild($editor, $editArticle); 
        $auth->addChild($editor, $manageTags); 
        $auth->addChild($editor, $seeRating); 
        $auth->addChild($editor, $seeStatus); 
        $auth->addChild($author, $seeStatus); 
  /*      $auth->addChild($author, $updateOwnUser); 
        $auth->addChild($updateOwnUser, $updateUsers);     
*/   }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180929_202052_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180929_202052_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
