<?php

use yii\db\Migration;

/**
 * Class m180927_150758_status_init
 */
class m180927_150758_status_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('status', [
            'name' => 'before editing',
        ]);
        $this->insert('status', [
            'name' => 'good for publishing',
        ]);

        $this->insert('status', [
            'name' => 'needs to return to author',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180927_150758_status_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180927_150758_status_init cannot be reverted.\n";

        return false;
    }
    */
}
