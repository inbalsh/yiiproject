<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php if (\Yii::$app->user->can('author')) { ?>

        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
  <!--      <p>
        <? //= Html::a('Show feedbacks', ['/article-feedback', 'id_article' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
-->
    <?php }
  else { ?>
    <?= Html::a('Rate this article', ['createfeed', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php } ?>
    </p>
    <p>
    <?php if (\Yii::$app->user->can('admin')) { ?>

    <?= Html::a('Rate this article', ['createfeed', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php } else if (\Yii::$app->user->can('editor')) { ?>

        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php } ?>
    </p>

<!-- הויו מכיר רק את המשתנים שהקונטרולר שלח לו-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'descriptin',
            'body:ntext',
         //   'author_id',
         //   'editor_id',    
            [
                'label' => 'Category',
                'format' => 'html',
                'value' => Html::a($model->category->name,
                ['article/index', 'ArticleSearch[category_id]' => $model->category_id]), 
            ],           

            [
                'label' => 'Tags',
                'format' => 'html',
                'value' => $tags,
            ],

            'created_at',
            'updated_at',
        /*    [
               'attribute' => 'created_at', 
               'value' => date('Y-m-d H:i:s', $model->created_at)
            ],
            [
               'attribute' => 'updated_at',
               'value' => date('Y-m-d H:i:s', $model->updated_at)
            ], */
           // 'created_by',
           [    // הופך את שם הכותב לקישור שמפנה לדף ביוזרס                  
            'label' => 'Author',
            'format' => 'html',
            'value' => Html::a($model->author1->name, 
                ['user/view', 'id' => $model->author1->id]), 
            ], 
           // 'updated_by',
           [
            'label' => 'Updated By',
            'format' => 'html',
            'value' => Html::a($model->editor1->name, 
                ['user/view', 'id' => $model->editor1->id]), 
            ], 
           
           [
                'label' => 'Status',
                'value' => $model->status1->name
            ],
            ],
    ]) ?>
<b>
For Related Article, Click on the Name of Category

</div>