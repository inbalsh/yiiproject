<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (\Yii::$app->user->can('writeArticle')) { ?>
        <p>

        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
</p>
    <?php } ?>


<?php 
if (\Yii::$app->user->can('seeStatus'))  { 
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
            'title',
            'descriptin',
         //   'body:ntext',
          //  'author_id',
            //'editor_id',
            [
                'label' => 'Author',
                'value' => 'author1.name'
            ], 
            [
            'label' => 'Category',
            'value' => 'category.name'
            ],
        //    'category_id',
            [
                'label' => 'Status',
                'value' => 'status1.name'
            ],
            'status_id',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        <?php } else { ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
            'title',
            'descriptin',
        //    'body:ntext',
          //  'author_id',
            //'editor_id',
            [
                'label' => 'Author',
                'value' => 'author1.name'
            ], 
            [
                'label' => 'Category',
                'value' => 'category.name'
            ],
            'category_id',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    }?>
        
</div>
