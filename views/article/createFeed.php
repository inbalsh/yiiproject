<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;
use app\models\Article;
use app\models\ArticleFeedback;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleFeedback */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="article-feedback-form">

<div class="ct1">

<?php if (Yii::$app->session->hasFlash('FeedSubmitted')): ?>

    <div class="row">
        <div class="col-lg-4">&nbsp;</div>
        <div class="col-lg-5">
            <div class="panel panel-default">
                <div class="panel-heading">Message Sent</div>
                <div class="panel-body">
                    <p><b>Your Rating:</b><?=$model->rating?></p>
                    <p><b>Your Message:</b><?=$model->message?></p>
                </div>
            </div>
            <div class="alert alert-success">
            Thank you for yout Feedback. We will respond as soon as possible
            </div>
            <p>
            <?= Html::a('Return to articles', ['index'], ['class' => 'btn btn-primary']) ?>
            </p>

        </div>
    </div>

<?php else: ?>

<div class="row ct">
    <div class="col-lg-3"></div>
        <div class="col-lg-6 panel panel-default bd">
            <div><h1 align="center"><?= Html::encode($this->title)?></h1></div>
            <?php $form = ActiveForm::begin(['id' => 'grievance-form']); ?>
          
        <?= $form->field($model, 'id_article')->hiddenInput(['value'=> $id])->label(false)?>

        <?= $form->field($model, 'author')->hiddenInput(['value'=> $author])->label(false)?>

                <?= $form->field($model, 'rating') ->widget(StarRating::classname(), ['pluginOptions' => ['size'=>'lg']]);?>

                <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php endif; ?>



    
</div>
