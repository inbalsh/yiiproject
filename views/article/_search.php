<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'globalSearch') ?>

    <?php //echo $form->field($model, 'id') ?>

    <?php //echo $form->field($model, 'title') ?>

    <?php //echo $form->field($model, 'descriptin') ?>

    <?php //echo $form->field($model, 'body') ?>

    <?php //echo $form->field($model, 'author_id') ?>

    <?php // echo $form->field($model, 'editor_id') ?>

    <?php // echo $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
          <!--      <? // echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?> -->
          <?= Html::a('reset', [''], ['class' => 'btn btn-default']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
