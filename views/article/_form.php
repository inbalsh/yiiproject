<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Status;
use dosamigos\selectize\SelectizeTextInput;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (\Yii::$app->user->can('author')) { ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'descriptin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>


<!--
    <? // echo $form->field($model, 'author_id')->textInput() ?>

    <? // echo $form->field($model, 'editor_id')->textInput() ?>
-->
    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'name')
    ) ?>
<!--
    <? // echo $form->field($model, 'created_at')->textInput() ?>

    <? // echo $form->field($model, 'updated_at')->textInput() ?>

    <? // echo $form->field($model, 'created_by')->textInput() ?>

    <? // echo $form->field($model, 'updated_by')->textInput() ?>
-->

    <?= $form->field($model, 'status_id')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'name'),['options'=>['2'=>['disabled'=>true], '3'=>['disabled'=>true]]]) ?>
     
       
        <?php } else if (\Yii::$app->user->can('editor')) { ?>

<?= $form->field($model, 'status_id')->dropDownList(
    ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'name'),['options'=>['1'=>['disabled'=>true], '2'=>['selected'=>true]]]
    )    ?>
    <?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
    // calls an action that returns a JSON object with matched
    // tags
    'loadUrl' => ['tag/list'],
    'options' => ['class' => 'form-control'],
    'clientOptions' => [
        'plugins' => ['remove_button'],
        'valueField' => 'name',
        'labelField' => 'name',
        'searchField' => ['name'],
        'create' => true,
    ],
])->hint('Use commas to separate tags') ?>

        <?php } ?>

    <div class="form-group">
    <p>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </p>
    </div>

    <?php ActiveForm::end(); ?>

</div>

