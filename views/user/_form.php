<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
 
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
<!--
    <? // echo $form->field($model, 'created_at')->textInput() ?>

    <? // echo $form->field($model, 'updated_at')->textInput() ?>

    <? // echo $form->field($model, 'created_by')->textInput() ?>

    <? // echo $form->field($model, 'updated_by')->textInput() ?>
-->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
