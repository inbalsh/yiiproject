<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleFeedback */

$this->title = $model->id_article;

//מציג קישור לדף הפידבקים של הכותב הספציפי
if (\Yii::$app->user->can('author')){
    $id = Yii::$app->user->id;
    $this->params['breadcrumbs'][] = ['label' => 'Article Feedbacks', 'url' => ['myfeed?author='.$id]];
}
//מציג קישור לדף הפידבקים של כל הכותבים
if (\Yii::$app->user->can('editor')){
    $this->params['breadcrumbs'][] = ['label' => 'Article Feedbacks', 'url' => ['index']];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php
if (\Yii::$app->user->can('admin')){ ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
        <?php } ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_article',
            'author',
            'article.title',
            'rating',
            'message',
        ],
    ]) ?>

</div>
