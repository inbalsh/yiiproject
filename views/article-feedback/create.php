<?php

use yii\helpers\Html;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleFeedback */

$this->title = 'Send Article Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Article Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="article-feedback-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
