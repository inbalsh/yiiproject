<?php

use yii\helpers\Html;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleFeedback */

$this->title = 'Update Article Feedback';
$this->params['breadcrumbs'][] = ['label' => 'Article Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="article-feedback-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
