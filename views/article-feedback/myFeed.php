<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Article Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
        //   'id_article',
        [
            'label' => 'Title Of Article',
            'value' => 'article.title'
        ], 
         //  'author',
          [
            'label' => 'Author',
            'value' => 'author1.name'
        ],   
            'rating',
            'message',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
