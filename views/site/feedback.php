<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

$this->title = 'Send your Feedback';
?>

<div class="ct1">


<?php
        if (\Yii::$app->user->can('admin'))  
{ ?>
<div class="feedback-index">     

    <h1>Feedbacks About My Knowledge Base</h1>
<br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

     //   'id',
        'subject',
        'message',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); 
}
else{?>     

    <?php if (Yii::$app->session->hasFlash('FeedSubmitted')): ?>

        <div class="row">
            <div class="col-lg-4">&nbsp;</div>
            <div class="col-lg-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Message Sent</div>
                    <div class="panel-body">
                        <p><b>Subject:</b><?=$model->subject?></p>
                        <p><b>Your Message:</b><?=$model->message?></p>
                    </div>
                </div>
                <div class="alert alert-success">
                Thank you for yout Feedback. We will respond as soon as possible
                </div>
            </div>
        </div>

    <?php else: ?>

    <div class="row ct">
        <div class="col-lg-3"></div>
            <div class="col-lg-6 panel panel-default bd">
                <div><h1 align="center"><?= Html::encode($this->title)?></h1></div>
                <?php $form = ActiveForm::begin(['id' => 'grievance-form']); ?>
                
                    <?= $form->field($model, 'subject') ?>

                    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    <?php endif; 
    
}?>

</div>

</div>
