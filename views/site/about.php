<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="body-content">

<div class="row">
    <div class="col-lg-6">
        <h2>About...</h2>

        <p>The Knowledgebase is a central repository of articles containing information
         that serves the needs of the target audience,
         In addiotion, Knowledgebase allowing each user to learn new topics or to refresh
          existing knowledge on familiar topics.</p>

    </div>

    <div class="col-lg-6">
        <h2>Services:</h2>

        <p>The KB system provides the following features:
        Search capability: The ability of the user to locate documents according to
        categories, keywords, keyword combinations, author name.
        Availability and accessibility:
        The ability of users to access information from anywhere, anytime they want.</p>

    </div>
</div>
