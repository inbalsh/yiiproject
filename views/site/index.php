<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\widgets\Menu;
use kartik\cmenu\ContextMenu;


/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to our <br> Travels Knowledge Base!</h1>
    </div>

    <div class="body-content">

<p>

<div class="row phc">
     <div class="col-lg-4">
        <a href="../web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=family tours">
       <h3>Family Tours</h3> 
       <img class="category" src="../image/giethoorn.jpg"> </a>
</div>
<div class="col-lg-4">
       <a href="../web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=urban tours">
       <h3>Trekking in Europe</h3> 
       <img class="category" src="../image/trek.jpg">     </a>   
   </div>
   <div class="col-lg-4">
       <a href="../web/index.php/article/index?ArticleSearch%5BglobalSearch%5D=trekking in europe">
       <h3>Urban Tours</h3> 
       <img class="category" src="../image/urban.jpg">        </a>
   </div>
   </div>

</p>
<br>
<br><br>
        <div class="row">
            <div class="col-lg-8">
            <h2 class='topic'>Help Topics</h2>

                <div class="col-lg-6">
                <p>
                    <?php    echo Menu::widget([
                    'items' => [
                            ['label' => 'My Account', 'url' => ['article/view?id=1']],
                            ['label' => 'Getting Started', 'url' => ['article/view?id=2']],
                    ], ]); ?>
                    </p>
                </div>

                <div class="col-lg-6">
                <p>
                    <?php    echo Menu::widget([
                    'items' => [
                            ['label' => 'GI KB', 'url' => ['article/view?id=3']],
                            ['label' => 'Support', 'url' => ['article/view?id=4']],
                    ], ]); ?>
                     </p>
                </div>
            </div>
            
            <div class="col-lg-4 popular">
                <h2>Popular Articles</h2>
                <br>
            <p>
                 <?php    echo Menu::widget([
                'items' => [
                        ['label' => 'Before Travle', 'url' => ['article/view?id=5']],
                        ['label' => 'Required Equipment', 'url' => ['article/view?id=6']],
                        ['label' => 'Top 10 Most Beautiful Places In The World', 'url' => ['article/view?id=7']],
                        ['label' => 'Two Weeks In Italy', 'url' => ['article/view?id=8']],
                ], ]); ?> 

                </p>

            </div>
    </div>
</div>
