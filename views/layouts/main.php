<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    /*
   if (Yii::$app->user->can('aau')) {
    $session = Yii::$app->session->getId();
    $user1->session = $session;
    $user1->save();
    }*/
   
    $id = Yii::$app->user->id; //מקבלים את איידי של המשתמש המחובר

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

// אם אמת הסוגרים הראשונים בתוקף, אם שקר הסוגריים השניים בתוקף
             Yii::$app->user->isGuest ? ( // אם אורח ייכנס להומ. אם יוזר ייכנס ליוזרס
            ['label' => 'Home', 'url' => ['../index.php']] //מה קורה אם התנאי מתקיים
             ):(
             ['label' => 'Users', 'url' => ['/user/index']] // מה קורה אם התנאי לא מתקיים
             ),

                [
                'label' => 'About',
                'url' => ['/site/about'],
                'visible' => Yii::$app->user->isGuest,
                ],

                
                [
                    'label' => 'Tags',
                    'url' => ['/tag/index'],
                    'visible' => Yii::$app->user->can('editor'),
                ],

                [
                    'label' => 'Categories',
                'url' => ['/category/index'],
                'visible' => Yii::$app->user->can('editor'),
                ],

                ['label' => 'Articles', 'url' => ['/article/index']],

                
                [
                    'label' => 'Feedbacks Of Articles',
                    'url' => ['/article-feedback/index'],
                    'visible' => Yii::$app->user->can('editor'),
                ],
            
                [
                    'label' => 'Feedbacks Of My Articles',
                    'url' => ['/article-feedback/myfeed?author='.$id], //שרשור עם איידי משתמש מחובר
                    'visible' => Yii::$app->user->can('author'),
                ],


            ['label' => 'Feedback', 'url' => ['/site/feedback']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; GI <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
