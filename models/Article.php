<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use dosamigos\taggable\Taggable;
use kartik\rating\StarRating;


/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $author_id
 * @property int $editor_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $status_id

 */
class Article extends \yii\db\ActiveRecord
{
  /**
     * @var string helper attribute to work with tags
     */
    public $tagNames;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */

    public function behaviors()
{
    return [
        [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
            'value' => new Expression('NOW()'),
        ],

    //השלוש שורות למטה זה כאשר רוצים לשנות את השם לקריאייטד ביי ואפדייטד ביי למשל. 
    //  אחרת אם משתמשים בשם הדיפולטיבי נשתמש רק בשורה שרשומה מתחת ל taggable
        [
            'class' => BlameableBehavior::className(),
            'createdByAttribute' => 'created_by',
            'updatedByAttribute' => 'updated_by',
        ],

        // for different configurations, please see the code
        // we have created tables and relationship in order to
        // use defaults settings
        Taggable::className(),
        //BlameableBehavior::className(),  הוספת שורה זו כדי שהקריאייט-בי ואפדייטד-ביי ימולאו באופן אוטומטי
    ];
}


    public function rules()
    {
        return [
            [['title'], 'required'],
            [['body'], 'string'],
            [['tagNames'], 'safe'],
            [['author_id', 'editor_id', 'category_id', 'status_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'descriptin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Descriptin',
            'body' => 'Body',
            'author_id' => 'Author ID',
            'editor_id' => 'Editor ID',
            'category_id' => 'Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'status_id' => 'Status ID',

        ];
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']); // שינוי האיי די לשם הקטגוריה - בשדה קטגוריה
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
    } // יוצר חיבור קישור בין המחלקה לבין התגיות

    public function getAuthor1()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }    

    public function getEditor1()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }    

    public function getStatus1(){
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    /* /חדש
    
    public function getArticleFeedback()
    {
        return $this->hasMany(ArticleFeedback::className(), ['id' => 'article_feedback_id'])->viaTable('afb', ['article_id' => 'id']);
    }

    public static function findAllByAttributes($id)
    {
         
        return Article::find()
        ->where(['like', 'id', $id])->limit(50)->all();
        }
*/

}