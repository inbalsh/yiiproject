<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ArticleFeedback;
use kartik\rating\StarRating;

/**
 * ArticleFeedbackSearch represents the model behind the search form of `app\models\ArticleFeedback`.
 */
class ArticleFeedbackSearch extends ArticleFeedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_article', 'author', 'rating'], 'integer'],
            [['message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticleFeedback::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_article' => $this->id_article,
            'author' => $this->author,
            'rating' => $this->rating,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

                // מיון יורד מדירוג גבוה לנמוך
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['rating' => SORT_DESC]],
          //  'sort'=> ['defaultOrder' => ['id_article' => SORT_DESC]]
            ]);

        return $dataProvider;
    }
}
