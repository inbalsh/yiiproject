<?php

namespace app\models;

use Yii;
use kartik\rating\StarRating;
use yii\db\Expression;

/**
 * This is the model class for table "article_feedback".
 *
 * @property int $id
 * @property int $id_article
 * @property int $author
 * @property double $rating
 * @property string $message
 */
class ArticleFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

  //  public $articleID;

    public static function tableName()
    {
        return 'article_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rating'], 'required'],
          //  [['articleID'], 'safe'],
            [['id_article', 'author'], 'integer'],
            [['rating'], 'double'],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_article' => 'Id Article',
            'author' => 'Author',
            'rating' => 'Rating',
            'message' => 'Message',
        ];
    }

    //חדש
    public function getAuthor1()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }    

    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'id_article']);
    }    
}
