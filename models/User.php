<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'user';
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
    
        //השלוש שורות למטה זה כאשר רוצים לשנות את השם לקריאייטד ביי ואפדייטד ביי למשל. 
        //  אחרת אם משתמשים בשם הדיפולטיבי נשתמש רק בשורה שרשומה מתחת ל taggable
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
    
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        //BlameableBehavior::className(),  הוספת שורה זו כדי שהקריאייט-בי ואפדייטד-ביי ימולאו באופן אוטומטי
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name', 'email', 'username', 'auth_key', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }


 // כל הפונקציות הבאות-מטרתן לקשר בין החלקות- שלנו ושל YII
 
 public static function findIdentity($id)
 {
     return static::findOne($id); // אם אנחנו מחוברים ורוצים לקבל עוד פרטים על היוזר
 }

 public static function findIdentityByAccessToken($token, $type = null)
 {
  //   return static::findOne(['access_token' => $token]);
 }

 public function getId()
 {
     return $this->id;
 }

 public function getAuthKey()
 {
     return $this->auth_key;
 }

 public function validateAuthKey($authKey)
 {
     return $this->auth_key === $authKey;
     // שלוש שווה- מאמת גם את סוג המשתנה, בדיקה יותר מחמירה
 }

 public static function findByUsername($username)
 {
     return self::findOne(['username'=>$username]);
     // בודקים במסד נתונים אם יש לנו יוזרניים כמו שקיבלנו
 }

  //צריך לממש ולידייט פסוורד
 // צריך לעשות משהו עם ההוסקי

 public function validatePassword($password){
    return \Yii::$app->security->validatePassword($password,$this->password);
 }


 public function beforeSave($insert){
     if(parent::beforeSave($insert)){
         if($this->isNewRecord){
             $this->auth_key = \Yii::$app->security->generateRandomString(); //פעולה המתבצעת פעם אחת כאשר יוצרים את היוזר
         }
         if($this->isAttributeChanged('password')){//מונע האש על האש
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
         }
         return true;
     }
     return false;
 }





    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
