<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public $globalSearch; // חיפוש גלובלי
    public $tag; // הגדיר תכונה שנקראת טאגס

     //תכונה שלא מופיעה פה, אני לא מעדכן אותה
    public function rules()
    {
        return [
            [['id', 'author_id', 'editor_id', 'category_id', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['title', 'globalSearch', 'descriptin', 'body', 'created_at', 'updated_at', 'tag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) // מקבלת את כל הפרמטרים שהיו בשורת ה url
                                    // והפכו לפרמטרים בכתיבה מובנת
    {
        $query = Article::find(); // סלקט כוכבית מארטיקל. מביא את כל המאמרים כי אין וור

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //$this->load($params);  // כעת מכיר את כל הפרמטרים
        //if (!$this->validate()) {

        if (!($this->load($params) && $this->validate())){

        // uncomment the following line if you do not want to return any records when validation fails
        // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([ // פונקציה שמוסיפה תנאים לקווארי קיים
            'id' => $this->id, // מדלג על התנאי אם הערך null
            'author_id' => $this->author_id,
            'editor_id' => $this->editor_id,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'status_id' => $this->status_id,

        ]);
            // פרמטרים מסוג טקסט, במקום שיווין מתייחס עם לייק
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descriptin', $this->descriptin])
            ->andFilterWhere(['like', 'body', $this->body]);
       //     ->andFilterWhere(['like', 'category_id', $this->category_id]);

        // חיפוש גלובלי
        $query->orFilterWhere(['like', 'title', $this->globalSearch])
        ->orFilterWhere(['like', 'descriptin', $this->globalSearch])
        ->orFilterWhere(['like', 'body', $this->globalSearch])
        ->orFilterWhere(['like', 'category_id', $this->globalSearch])
        ->orFilterWhere(['like', 'category.name', $this->globalSearch])
     //=   ->orFilterWhere(['like', 'status.name', $this->globalSearch])
           ->orFilterWhere(['like', 'status_id', $this->globalSearch]);
        //->orFilterWhere(['like', 'created_by', $this->globalSearch]);
        
        //add tags condition
        if (!empty($this->tag))
        {
            // רוצים לראות את כל המאמרים שמתוייגים באותה תגית

            $condition = Tag::find()->select('id')->where(['IN' , 'name' , $this->tag]); //יודע להפעיל על הטבלה טאג מהמחלקה טאג
                                                                                        // יוציא את האיי די של התגית שמופיעה ב url
            $query->joinWith('tags'); // מקבל ג'וין של הארטיקל . מופיע כתכונה כי זה פונקציית גטר
            $query->andWhere(['IN' , 'tag_id' , $condition]); //נחפש את אלה שהטאג איי די שלהם נמצא בקונדישן ונוסיף תנאי לקווארי שעושה סינון
                  //here the condition will apear
      
        }

        $query->joinWith(['category']); 
        $dataProvider->sort->attributes['category_id'] = [ 
            'asc' => ['Category.name' => SORT_ASC],
            'desc' => ['Category.name' => SORT_DESC],
        ];       

/*
        $query->joinWith(['status']); 
        $dataProvider->sort->attributes['status_id'] = [ 
            'asc' => ['Status.name' => SORT_ASC],
            'desc' => ['Status.name' => SORT_DESC],
        ];   
*/
        return $dataProvider;
    }
}
