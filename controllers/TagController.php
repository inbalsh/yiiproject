<?php

namespace app\controllers;

use Yii;
use app\models\Tag;
use app\models\TagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\web\response;
use yii\filters\AccessControl;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'access' => [ // כאן מגדירים למי מותר להיכנס לאן
                'class' => AccessControl::className(),
                'only' => ['index','view','update','create','delete'], 
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','update','create','delete'], 
                        'roles' => ['manageTags'], 
                    ],
                ],
        ],
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Tag();

      //  if (\Yii::$app->user->can('manageTags',['tag' => $model])){
        
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
      //  }
      //  throw new ForbiddenHttpException('Sorry, you are not allowed to add tags');       
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
 //       if (\Yii::$app->user->can('manageTags')){
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
    //    }
   //     throw new ForbiddenHttpException('Sorry, you are not allowed to update tags');
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
 //       if (\Yii::$app->user->can('manageTags')){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
   //    }
 //   throw new ForbiddenHttpException('Sorry, you are not allowed to delete tags');
    }



    // actionList to return matched tags
    public function actionList($query) //הדולר קווארי מקבל אוסף של תווים, אנו נפנה לדטה בייס  כדי לשלוף תגיות שמתאימות לתווים.
    {                                       // צריכה להיות התאמה בין הדולר קווארי לבין מה שנכתב ב url
        $models = Tag::findAllByName($query); // מודל זה כל האובייקטים של התגים. נלך למחלקה טאג ונפעיל עליה פונקציה פיינדאולבייניימ.
                                                //קיבלנו רשימת כל התגים שמתחילים באוסף התווים שנמצא בקווארי. זהו מערך של אובייקטים
        $items = [];                // מערך ריק שבהמשך נוסיף לו איבר כל פעם

        foreach ($models as $model) {
            $items[] = ['name' => $model->name];    // הוספת איבר למערך. הניימ הראשון הוא סטרינג. הניים השני הוא תכונה.
        }
        // We know we can use ContentNegotiator filter
        // this way is easier to show you here :)
        Yii::$app->response->format = Response::FORMAT_JSON; // נרצה שיחזיר בפורמט גייסון ואז יחזיר את מערך האייטמס

        return $items;
    }


    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
