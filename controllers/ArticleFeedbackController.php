<?php

namespace app\controllers;

use Yii;
use app\models\ArticleFeedback;
use app\models\ArticleFeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\filters\AccessControl;
use kartik\rating\StarRating;

/**
 * ArticleFeedbackController implements the CRUD actions for ArticleFeedback model.
 */
class ArticleFeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'access' => [ // כאן מגדירים למי מותר להיכנס לאן
                'class' => AccessControl::className(),
                'only' => ['index', 'view','delete'], // חל רק על עדכון. ניתן להוסיף עוד בתוך הסוגריים עם הפרדת פסיקים
                'rules' => [
                    [
                        'allow' => true, //תאפשר
                        'actions' => ['index', 'view'], // לעשות עדכון
                        'roles' => ['editor'], //                     
                    ],
                       [
                        'allow' => true, //תאפשר
                        'actions' => ['view'],
                        'roles' => ['author'], 
                        ],
                        
                        [
                            'allow' => true, //תאפשר
                            'actions' => ['delete'],
                            'roles' => ['deleteFeedback'], 
                            ],

                        ],
                    ],
        ];
    }

    /**
     * Lists all ArticleFeedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleFeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

// דף לצפייה בפידבקים של כתבות של כותב מסויים
    public function actionMyfeed($author)
    {

        
       $id = Yii::$app->user->id; //מקבלים את איידי של המשתמש המחובר  
        if ($id == $author){ // רק אם איידי של משתמש מחובר שווה לכותב הכתבה

        $searchModel = new ArticleFeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['author'=>$author]);  
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    else
    {
        throw new ForbiddenHttpException('You are not allowed to perform this action.');

    }

    }






    /**
     * Displays a single ArticleFeedback model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticleFeedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
  /*  public function actionCreate()
    {
        $model = new ArticleFeedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('FeedSubmitted');

            return $this->redirect(['view', 'id' => $model->id]);
        }
        else {


        return $this->render('create', [
            'model' => $model,
        
        ]);
        }}
    
*/
            
     

    /**
     * Updates an existing ArticleFeedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
 /*   public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    } 
    */ 

    /**
     * Deletes an existing ArticleFeedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticleFeedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleFeedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleFeedback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
